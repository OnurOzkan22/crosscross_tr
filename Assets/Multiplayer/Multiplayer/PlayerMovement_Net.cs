﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using UnityEngine.Networking;

public class PlayerMovement_Net : NetworkBehaviour {

	[SyncVar]
	public int myScore;

	[SyncVar]
	public int playerNumber;

	public GameObject buttonPanel;
	[Header("Player Animator")]
	public Animator playerAnim;
	public SpriteRenderer rend;
	[Header("Touch Control")]
	[Tooltip("Aktiviert Touch Control")]
	public bool touchStart = false;
	public static bool stopTouch = false;
	[Header("Game Over Screen")]
	[Tooltip("Bindet Restart Button ein und löst ihn aus")]
	public GameObject restartButton;
	[Header("Einbindung StartButton")]
	public GameObject startButton;
	public StartButton startScript;
	[Tooltip("hier eingeben welchen Speed der Player nach der Idle haben soll. Eingeben welchen Speed der Player vorher hatte! bsp playerSpeed = -7 dann afterIldeSpeed = -7")]
	public float afterIdleSpeed; 
	public float getPlayerSpeed;
	public bool walkNow;
	[Header("NoTouch Achievment if true unlock")]
	public bool noTouch = false; // für Achviement das du nicht getoucht hast un zum counter gekommen bist
	[Header("CoinsFeedback")]
	public GameObject dollarsPlus1;
	public GameObject goldNuggetsPlus1;
	public CircleCollider2D box;
	public Rigidbody2D rigid;


	// Use this for initialization
	void Start () {

		rigid = GetComponent<Rigidbody2D> ();

		buttonPanel = GameObject.Find ("ButtonPanel");

		box = GetComponent<CircleCollider2D> ();

		rend = GetComponent<SpriteRenderer> ();

		//playerContainer = GameObject.FindGameObjectWithTag ("PlayerContainer");
		//script = playerContainer.GetComponent<PlayerContainer> ();

		startButton = GameObject.FindGameObjectWithTag ("StartButton");
		startScript = startButton.GetComponent<StartButton> ();
	
		playerAnim = GetComponent<Animator> ();

		if (isServer) {
		
			CmdGetPlayerNummbers (1);


		} else {
		
			CmdGetPlayerNummbers(2);
		}

		if (!isLocalPlayer) {

			//Destroy (this);
			return;
		}
			
	}
	
	// Update is called once per frame
	void Update () {

		#region code in Update

		///Vorwarnung für extra Life 
		//playerAnim.SetBool("speed", walkNow);
	
		////////////////////////////////////////////////////////////////////////////////////////////////

		//spezifischer speed wird an den Player Container übergeben
		/*if (startScript.startPlayerContainer == true) {

			script.playerSpeed = getPlayerSpeed;

		}*/

		rigid.velocity = new Vector2(getPlayerSpeed,rigid.velocity.y);
		//////////////////////////////////////////////////////////////

		///Touch Steuerung
		if (touchStart == true/*aktiviert TouchControl*/) {
			foreach (Touch touch in Input.touches) {

				if (touch.phase == TouchPhase.Began && stopTouch == false) {

					getPlayerSpeed = 0;
					noTouch = true;
					walkNow = false;
				} 
			
				if (touch.phase == TouchPhase.Ended) {
					walkNow = true;
					getPlayerSpeed = afterIdleSpeed; 
				}
			}
		} 
		///////////////////////////////////////////////////////////////////////////////////////////////	

		//Debug.Log (script.deathCounter);

		#endregion
	}
				
	void OnTriggerEnter2D (Collider2D coll)
	{

		if (coll.gameObject.tag == "GoldNugget") {
		
			Debug.Log ("yes hit");
			Destroy (coll.gameObject);
			Instantiate (goldNuggetsPlus1, transform.position, Quaternion.identity);
		}

		if (coll.gameObject.tag == "Coin") {

		
			Destroy (coll.gameObject);
			Instantiate (dollarsPlus1, transform.position, Quaternion.identity);

		}

		if (coll.gameObject.tag == "Counter") {

			transform.position = new Vector2 (9.14f, -0.97f);
		

			//CmdGetPoint ();
			//playerIsComing.SetActive (true);
		}

		if (coll.gameObject.tag == "Car") {

			transform.position = new Vector2 (9.14f, -0.97f);
		}

	}
		
	//Server sendet daten an Clients
	[ClientRpc]
	void RpcShowToAll(int score){



	}

	//Clients senden daten an Server
	[Command]
	void CmdGetPlayerNummbers(int myID){

		playerNumber = myID;
	}
		

}
	//////////////////////////////////////////////////////////////////////////////////////////

	



