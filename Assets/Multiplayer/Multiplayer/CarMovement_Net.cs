﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class CarMovement_Net : NetworkBehaviour {

	//public Transform sightStart, sightEnd;
	//public bool spotted;

	public LayerMask CheckLayer;
	public bool isCollided;
	Vector2 checkCarBox;

	[Header("Geschwindigkeit des Cars")]
	public float speed; 
	[Header("Geschwindigkeits Index")]
	[Tooltip("Hier Geschwindigkeitszustände eintragen die angenommen werden sollen je nach Score")]
	public int[] speedIndex;
	Rigidbody2D rigid;
	[Header("PlayerContainer Script Einbindung")]
	public GameObject playerContainer;
	public PlayerContainer script;
	[Header("Player Script Einbindung")]
	public GameObject player;
	public PlayerMovement playerScript;
	[Header("SpeedLimit Script Einbindung")]
	public GameObject speedLimit;
	public SpeedLimit speedLimitScript;
	[Header("Einbindung RandomBackground für Animation des Autos")]
	public GameObject randomBackground;
	public GetBackground randomBackScript;

	[Header("Animations Einstellungen")]
	public Animator anim;
	[Tooltip("Wenn true dann schaltet Script die Animator komponennte ein(Auschalten bei Objeckten die keine Anims haben")]
	public bool getAnimator = true;
	[Tooltip("Hier Anim Namen eingeben und script jeweils erweitern wenn neue dazu kommen")]
	public string[] animNamesToPlay;

	// Use this for initialization
	[ServerCallback]
	void Start () {

		if (getAnimator == true) {
			anim = GetComponent<Animator> ();
		}

		rigid = GetComponent<Rigidbody2D>();

		//speedLimit = GameObject.FindGameObjectWithTag ("SpeedLimit");
		//speedLimitScript = speedLimit.GetComponent<SpeedLimit> ();

		//playerContainer = GameObject.FindGameObjectWithTag ("PlayerContainer"); 
		//script = playerContainer.GetComponent<PlayerContainer> ();

		//player = GameObject.FindGameObjectWithTag ("Player"); 
		//playerScript = playerContainer.GetComponent<PlayerMovement> ();

		checkCarBox = new Vector2 (0.5f, 0.5f);

		speed = speedIndex [1];

		if (transform.position.y > 0) {

			Debug.Log ("UMKERH");
			 speed*= -1;
		} 

		//Detail Anim die abgespielt werden soll
		/*if (getAnimator == true) {

			if (randomBackScript.getThisBack == 0) {

				anim.Play (animNamesToPlay[0]);
			}*/


			// hier weiterführen wenn mehr Anims benötigt werden


		}
			

	[ServerCallback]
	void Update (){   

		isCollided = Physics2D.OverlapBox (transform.GetChild(0).gameObject.transform.position, checkCarBox,2,CheckLayer);

		if (isCollided) {
		
			if (transform.position.x < 0) {

				speed = -4;
		
			} else {

				speed = 4;

			}


		}

		//// Autos werden schneller abhängig vom score
		/*if (script.score > 7) {

			//Checkt ob oben Gespawnt wird oder unten je nach dem wird der Speed negativ oder positiv genommen
			/*if (transform.position.x < 0) {

				speed = -speedIndex [2];

			} else {
			
				Debug.Log ("YES");
				speed = speedIndex [2];
			 
			}
		}
			
		if (script.score > 17) {
			//Checkt ob oben Gespawnt wird oder unten je nach dem wird der Speed negativ oder positiv genommen
				if (transform.position.x < 0) {

					speed = -speedIndex [3];

				} else {

					Debug.Log ("YES");
					speed = speedIndex [3];

				}


			}

		//Checkt ob ob das dreißiger Item eingesammelt wurde und verlangsamt die Autos
		if (speedLimitScript.slow == true) {


		if (transform.position.x < 0) {
				speed = -speedIndex [0];

			} else {

				Debug.Log ("YES");
				speed = speedIndex [0];

			}
	}*/

		rigid.velocity = new Vector2 (rigid.velocity.x, speed); // Gibt dem Car eine Bewegung

}


		
	// Collisions Erkennung (Abstand vom vorderen Auto halten)
	/*void OnCollisionEnter2D (Collision2D coll){
	
		if (coll.gameObject.tag == "Car") {


			if (transform.position.x < 0) {
				speed = -4;

			} else {

				//Debug.Log ("YES");
				speed = 4;

			}
		}


	}*/

	void OnBecameInvisible(){
	
		NetworkServer.Destroy (gameObject);
	}

	#if UNITY_EDITOR
	void OnDrawGizmos(){

		Gizmos.color = Color.blue;
		checkCarBox = new Vector2 (0.5f, 0.5f);
		isCollided = Physics2D.OverlapBox (transform.GetChild(0).gameObject.transform.position, checkCarBox,2,CheckLayer);
		Gizmos.DrawWireCube (transform.GetChild(0).gameObject.transform.position, checkCarBox);
	}
	#endif

}



		
	
		
	





	
	

