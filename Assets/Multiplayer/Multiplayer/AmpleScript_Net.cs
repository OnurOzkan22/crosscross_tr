﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class AmpleScript_Net : NetworkBehaviour {

	[Header("Einbindung ZugSpawnManager")]
	public GameObject zugSpawnManager;
	public ZugSpawnManager_Net zugScript;
	public Animator anim;
	public string[] whichAnim;

	// Use this for initialization
	void Start () {
	
		if (!isLocalPlayer) {
		
			Destroy (this);
			return;
		
		}

		zugSpawnManager = GameObject.FindGameObjectWithTag ("ZugSpawnManager");
		zugScript = zugSpawnManager.GetComponent<ZugSpawnManager_Net> ();

		anim = GetComponent<Animator> ();

	}
	
	// Update is called once per frame
	void Update () {
	
		if (zugScript.ampelSchalten == true) {
		
			anim.Play (whichAnim[0]);
		} else if (zugScript.ampelSchalten == false) {

			anim.Play (whichAnim[1]);
		} 
	}
}
