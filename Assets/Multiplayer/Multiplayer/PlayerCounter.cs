﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class PlayerCounter : NetworkBehaviour {

	[SyncVar]
	public int serverScore;
	[SyncVar]
	public int clientScore;

	public Text serverText;
	public Text clientText;


	// Use this for initialization
	void Start () {

		serverText.text = serverScore.ToString ();
		clientText.text = clientScore.ToString ();

	}
	
	// Update is called once per frame
	void Update () {


	}

	[ServerCallback]
	void OnTriggerEnter2D(Collider2D coll){
	
	if (coll.gameObject.tag == "Player") {

			//HIEEEEEEER synchroniesiert er noch nicht richtig der server macht er aber nicht den client
			Debug.Log (coll.gameObject.GetComponent<PlayerMovement_Net> ().playerNumber + "NUUUUUUUUUUMMMER");

			if(coll.gameObject.GetComponent<PlayerMovement_Net>().playerNumber == 2){

				clientScore++;
				CmdScore (serverScore, clientScore);

			} else if (coll.gameObject.GetComponent<PlayerMovement_Net>().playerNumber == 1){

				serverScore++;
				CmdScore (serverScore, clientScore);


			} 



		//playerIsComing.SetActive (true);
		}
	}

	[Command]
	void CmdScore(int cmdServerScore, int cmdClientScore){

		serverText.text = cmdServerScore.ToString();
		clientText.text = cmdClientScore.ToString ();
		RpcScore (cmdServerScore,cmdClientScore);

	}

	[ClientRpc]
	void RpcScore(int rpcServerScore, int rpcClientScore){

		serverText.text = rpcServerScore.ToString();
		clientText.text = rpcClientScore.ToString ();

	}
		

}
