﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class SpawnManagment_Net : NetworkBehaviour {

	[Header("Spawn System Cars")]
	[Tooltip("Array für GameObject kann beliebig erweitert werden")]
	public GameObject[] cars;
	[Tooltip("Bestimmt Random ein Auto")]
	public int random;
	[Tooltip("Wenn null dann Spawnt ein neues Auto")]
	public float timer; 
	[Tooltip("Setzt Timer auf random Wert zurück")]
	public float globalTimerReset;
	[Tooltip("Startet Das Spawnen wieder nach dem die 30iger Zone wieder deaktiviert ist")]
	public bool startSM = true;
	[Header("Arrow anzeigen wenn Auto Spawnt")]
	public GameObject arrowAttention;
	[Tooltip("Wir ausgeführt sobald dieser Wert größer ist als timer")]
	public float showArrowTimer;
	public int ArrayPos;
	public int ArrayPos2;


	void Awake (){

		//gameObject.SetActive (false);
	}

	// Use this for initialization
	void Start () {

		if (!isLocalPlayer) {
		

			return;
		}


	}
	
	[ServerCallback]
	void Update ()
	{

		if (isServer) {
			timer -= Time.deltaTime;
			//Spawnt wenn timer 0 ist Random autos mit den jeweiligen Arrays
			if (0 > timer && startSM == true) {
			


				//Aktiviert Warnsignal bei schnellen Autos (bis jetzt bei blauen) /!!! Erstmal rausgeschmissen nicht nötig(Csongor)
				if (showArrowTimer < 1 && random == ArrayPos || random == ArrayPos2) {

					//arrowAttention.SetActive (true);
				} else {

					//arrowAttention.SetActive (false);

				}
		
				random = Random.Range (0, 5);
				CmdInstantiateCar ();
			

				globalTimerReset = Random.Range (1.0f, 1.2f); // generiert einen Random wert für Timer
				timer = globalTimerReset; // übergibt diesen Wert an Timer
			}
		}

	}

	[Command]
	void CmdInstantiateCar(){

		GameObject newCar = (GameObject)Instantiate (cars [random], transform.position, Quaternion.identity); //Spawnt das Object
		NetworkServer.Spawn(newCar);
		Debug.Log("HIIIIIIIIIIIIIIII");
	}
}

