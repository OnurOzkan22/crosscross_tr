﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class ZugMovement_Net : NetworkBehaviour {

	[Header("Zug Geschwindigkeit")]
	[Tooltip("Hier können verschiedene Geschwindigkeiten angegeben werden")]
	public int[] speedIndex; //kann noch eingebaut werden
	[Tooltip("Die Geschwindigkeit des Zuges")]
	public float speed;
	Rigidbody2D rigid;


	// Use this for initialization
	void Start () {
	
		rigid = GetComponent<Rigidbody2D>();

		if (transform.position.y > 0) {

			speed *= -1;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
		rigid.velocity = new Vector2 (rigid.velocity.x, speed);
	}

	void OnBecameInvisible(){

		Destroy (gameObject);
	}
}
