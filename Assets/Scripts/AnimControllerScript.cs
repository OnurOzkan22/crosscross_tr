﻿using UnityEngine;
using System.Collections;

public class AnimControllerScript : MonoBehaviour {

	public GameObject startButton;
	public StartButton script;
	public Animator anim;
	public string animationName;

	// Use this for initialization
	void Start () {
	
		startButton = GameObject.FindGameObjectWithTag ("StartButton");
		script = startButton.GetComponent<StartButton> ();

		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
	
		if (script.startPanelAnim == true) {
		
			anim.Play (animationName);
		}
	}
}
