﻿using UnityEngine;
using System.Collections;

public class CounterScript : MonoBehaviour {

	public Animator anim;
	public GameObject yeahText;
	public GameObject door;
	public Camera cam;
	public ShakingScreen shakeScript;
	public AudioSource enterGoalSound;

	// Use this for initialization
	void Start () {
	
		cam = Camera.main;
		shakeScript = cam.GetComponent<ShakingScreen> ();
		enterGoalSound = GetComponent<AudioSource> ();

	}
	
	// Update is called once per frame
	void Update () {
	
		door = GameObject.FindGameObjectWithTag ("Door");
		anim = door.GetComponent<Animator> ();

	}

	void OnTriggerEnter2D (Collider2D coll){

		if (coll.gameObject.tag == "PlayerContainer") {
		
			anim.Play ("DoorOpen");
			enterGoalSound.Play ();
			shakeScript.Shake (0.1f, 0.2f);
			yeahText.SetActive (true);
		}
	}


}
