﻿using UnityEngine;
using System.Collections;

public class HowToPlay : MonoBehaviour {

	public float timer;
	public float resetTimer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		timer -= Time.deltaTime;

		if (1 > timer) {

			timer = resetTimer;
			gameObject.SetActive (false);

		}
	}
}
