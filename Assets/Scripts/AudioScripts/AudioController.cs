﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour {

	public AudioSource[] getSound;
	public static AudioSource[] sounds;

	public static AudioClip[] getThemes;
	public AudioClip[] themes;
	public static AudioSource myAudio;

	// Use this for initialization
	void Start () {

		myAudio = GetComponent<AudioSource> ();

		getThemes = new AudioClip[themes.Length];

		for (int i = 0; i < themes.Length; i++) {

			getThemes [i] = themes [i];
		}

		sounds = new AudioSource[getSound.Length];

		for (int i = 0; i < getSound.Length; i++) {
		
			sounds [i] = getSound [i];
		}

		myAudio.clip = getThemes [PlayerContainer.selectionIndex];
		myAudio.Play ();

	}
	
	public static void GetThisThemeOnStart(int index){
	
		myAudio.clip = getThemes [index];
		myAudio.Play ();
	}

	public static void GetTheme(int index){

		myAudio.clip = getThemes [index];
		myAudio.Play ();
	}

	public static void StopTheme(){

		myAudio.Stop ();
		sounds [0].Play ();
	}

	public void PlayButtonSound(){

		sounds [1].Play ();

	}

	public void PlayItemButtonSound(){

		sounds [2].Play ();
	}


}
