﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScrollCharacterEditor : MonoBehaviour {

	public RectTransform panel;
	public Button[] button;
	public RectTransform center;

	private float[] distance;
	private bool dragging = false;
	private int buttonDistance;
	private int minButtonNum;
	public AudioSource scroll;

	// Use this for initialization
	void Start () {
	
		//panel = GetComponent<RectTransform> ();
		scroll = GetComponent<AudioSource>();

		int buttonLenght = button.Length;
		distance = new float[buttonLenght];
		buttonDistance = (int)Mathf.Abs (button [1].GetComponent<RectTransform> ().anchoredPosition.x - button [0].GetComponent<RectTransform> ().anchoredPosition.x);
	}
	
	void Update(){

		for(int i = 0; i < button.Length; i++){

			distance [i] = Mathf.Abs (center.transform.position.x - button [i].transform.position.x);
		}

		float minDistance = Mathf.Min (distance);

		for (int a = 0; a < button.Length; a++) {
		
			if (minDistance == distance [a]) {
			
				minButtonNum = a;
			}
		}

		if (!dragging) {
		
			LerpToButton (minButtonNum * -buttonDistance);

		}
	}

	void LerpToButton(int position){

		float newX = Mathf.Lerp (panel.anchoredPosition.x, position, Time.deltaTime * 10f);
		Vector2 newPos = new Vector2 (newX, panel.anchoredPosition.y);
		panel.anchoredPosition = newPos;
	}

	public void StartDrag(){

		dragging = true;


	}

	public void EndDrag(){
	
		dragging = false;
		scroll.Play ();
	
	}

	public void PlayScrollSound(){

		//scroll.Play ();
	}
}
