﻿using UnityEngine;
using System.Collections;

public class SaveSelectedPalyer : MonoBehaviour {

	public GameObject[] image; //hier image von Spieler einfügen bei erweiterung
	public GameObject playerMarker;
	public GameObject playerContainer;
	public PlayerContainer script;
	public float xPos;
	Vector2 newPos;
	public RectTransform rect;


	// Use this for initialization
	void Start () {


		rect = GetComponent<RectTransform> ();

		playerContainer = GameObject.FindGameObjectWithTag ("PlayerContainer");
		script = playerContainer.GetComponent<PlayerContainer> ();
	
	}

	
	// Update is called once per frame
	void Update ()
	{
	

		xPos = transform.position.x;

		/*if (thisPlayer == 0) {

			script.Select (0);
			playerMarker.transform.position = image[0].transform.position;
			playerMarker.transform.SetParent (image [0].transform);

		}

		if (thisPlayer == 1) {

			script.Select (1);
			playerMarker.transform.position = image[1].transform.position;
			playerMarker.transform.SetParent (image [1].transform);


		}

		if (thisPlayer == 2) {

			script.Select (2);
			playerMarker.transform.position = image[2].transform.position;
			playerMarker.transform.SetParent (image [2].transform);

		}

		if (thisPlayer == 3) {

			script.Select (3);
			playerMarker.transform.position = image[3].transform.position;
			playerMarker.transform.SetParent (image [3].transform);

		}

		if (thisPlayer == 4) {

			script.Select (4);
			playerMarker.transform.position = image[4].transform.position;
			playerMarker.transform.SetParent (image [4].transform);

		}*/
		//Hier erweitern bei mehreren Playern
	}

	public void MarkThisPlayer(){
	


		script.Select (PlayerContainer.selectionIndex);
		playerMarker.transform.position = image[PlayerContainer.selectionIndex].transform.position;
		playerMarker.transform.SetParent (image [PlayerContainer.selectionIndex].transform);

	}

	public void GetFirstStartPos(){
	
		if (PlayerContainer.selectionIndex == 0) {


			rect.anchoredPosition = new Vector2 (0, 0);

		} else if (PlayerContainer.selectionIndex > 0) {

			xPos = PlayerPrefs.GetFloat ("SavePosXPlayer");

			transform.position = new Vector2 (xPos, transform.position.y);

		}
	}
		
}
