﻿//Diese Script auf ein neuen Button legen und die Werte ausfüllen
//im script SaveSelectedPlayer das Script erweitern


using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SelectPlayer : MonoBehaviour {

	[Header("Standart Player?")]
	public bool standartPlayer;
	//Ober klasse Charakter erben von dieser für unlock und locked //Geht noch nicht
	[Header("Einbindung Button Panel")]
	public GameObject buttonPanel;
	public SaveSelectedPalyer buttonScript;
	[Header("Einbindung PlayerContainer")]
	public GameObject playerContainer;
	public PlayerContainer script;
	[Header("Einbindung BackgroundArray")]
	public GameObject background;
	public GetBackground backScript;
	[Header("PlayerMarker einfügen")]
	public GameObject playerMarker;
	[Header("PlayerImage einfügen")]
	public GameObject image;
	public Animator anim;
	[Header("Button Eigenschaften")]
	public Text buttonText;
	[Tooltip("Text wenn Player Unlocked ist")]
	public string ifUnlocked; 
	[Header("Wenn True play unlocked Anim")]
	public bool playAnimUnlocked;
	//public string saveKey;

	[Header("Welcher Player soll beim Start ausgewählt sein")]
	public float getThisPlayerOnStart; //speicher variable merkt sich spieler und sucht ihn aus srpingt nicht immer wieder auf ersten spieler Charakterauswahl

	public bool getPosKnow = false;

	//Abfrage Variablen wenn Player ausgewählt wurde zum speichern
	[Header("Speichert unlocked oder locked 0 = Locked/1= Unlocked")]
	public float isSelectedSave = 0; 
	[Header("Unlock Preis")]
	public int unlockPrice;
	[Header("Welcher Player soll ausgewählt werden beim klicken auf Button")]
	public int SelectPlayerArray = 0;
	public int thislevel;


	// Use this for initialization
	void Start () {

		Init ();
		//PlayerPrefs.DeleteAll ();

	}
	
	// Update is called once per frame
	void Update () {

		anim.SetFloat ("isUnlocked", isSelectedSave);
		anim.SetInteger("LevelUp", thislevel);
		//anim.SetInteger("LevelUp", playerContainer.transform.GetChild(transform.GetSiblingIndex()).GetComponent<PlayerMovement>().level);
		ButtonText ();

	}

	public virtual void Player (){



		if (script.coins >= unlockPrice && isSelectedSave == 0) {

			//gameObject.GetComponent<Image> ().color = Color.gray;
			getPosKnow = true;
			script.Select (transform.GetSiblingIndex());
			backScript.Select (transform.GetSiblingIndex());
			isSelectedSave = 1;
			Debug.Log (isSelectedSave);
			script.coins -= unlockPrice;
			PlayerPrefs.SetFloat ("Coins", script.coins);
			PlayerPrefs.SetFloat (gameObject.name, isSelectedSave);
			Debug.Log ("Unlocked");
			playerMarker.transform.position = image.transform.position;
			playerMarker.transform.SetParent(image.transform);
			AudioController.GetTheme (PlayerContainer.selectionIndex);
			//Freischalt sound

			//Erst wenn auf den button geklcikt wird speichert er die xPos 
			if (getPosKnow == true) {
			
				PlayerPrefs.SetFloat ("SavePosXPlayer", buttonScript.xPos);
			}

		} else if(script.coins < unlockPrice && isSelectedSave == 0){
		
			//Debug.Log ("Not enough Coins");

			//nicht freischaltbar

		} else if(isSelectedSave == 1){
			
			getPosKnow = true;
			script.Select (transform.GetSiblingIndex());
			backScript.Select (transform.GetSiblingIndex());
			buttonText.text = ifUnlocked;
			playerMarker.transform.position = image.transform.position;
			playerMarker.transform.SetParent(image.transform);
			AudioController.GetTheme (PlayerContainer.selectionIndex);
			//auswahl sound

			//Erst wenn auf den button geklcikt wird speichert er die xPos um beim start das ButtonPanel auf die richtige höhe zu scrollen
			if (getPosKnow == true) {

				PlayerPrefs.SetFloat ("SavePosXPlayer", buttonScript.xPos);
			} 

		}


	}

	public virtual void Init ()
	{

		playerContainer = GameObject.FindGameObjectWithTag ("PlayerContainer");
		script = playerContainer.GetComponent<PlayerContainer> ();

		buttonPanel = GameObject.FindGameObjectWithTag ("ButtonPanel");
		buttonScript = buttonPanel.GetComponent<SaveSelectedPalyer> ();

		background = GameObject.FindGameObjectWithTag ("RandomBackground");
		backScript = background.GetComponent<GetBackground> ();

		isSelectedSave = PlayerPrefs.GetFloat (gameObject.name);

		thislevel = PlayerPrefs.GetInt (gameObject.name + "MenuImageAnim");

		anim = image.GetComponent<Animator> ();

	}

	public virtual void ButtonText(){


		if (!standartPlayer) {
			if (isSelectedSave == 1) {


				buttonText.text = ifUnlocked;


			} else if (isSelectedSave < 1) {
			
				
				buttonText.text = unlockPrice + " C";
				
			}
		} else {
		
			buttonText.text = "SELECT";
		}

		}
	}

