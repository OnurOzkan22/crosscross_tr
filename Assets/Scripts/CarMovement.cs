﻿using UnityEngine;
using System.Collections;

public class CarMovement : MonoBehaviour {

	//public Transform sightStart, sightEnd;
	//public bool spotted;

	public LayerMask CheckLayer;
	public bool isCollided;
	Vector2 checkCarBox;

	[Header("Geschwindigkeit des Cars")]
	public float speed; 
	[Header("Geschwindigkeits Index")]
	[Tooltip("Hier Geschwindigkeitszustände eintragen die angenommen werden sollen je nach Score")]
	public int[] speedIndex;
	Rigidbody2D rigid;
	[Header("PlayerContainer Script Einbindung")]
	public GameObject playerContainer;
	public PlayerContainer script;
	[Header("Player Script Einbindung")]
	public GameObject player;
	public PlayerMovement playerScript;
	[Header("SpeedLimit Script Einbindung")]
	public GameObject speedLimit;
	public SpeedLimit speedLimitScript;
	[Header("Einbindung RandomBackground für Animation des Autos")]
	public GameObject randomBackground;
	public GetBackground randomBackScript;

	[Header("Animations Einstellungen")]
	public Animator anim;
	[Tooltip("Wenn true dann schaltet Script die Animator komponennte ein(Auschalten bei Objeckten die keine Anims haben")]
	public bool getAnimator = true;

	// Use this for initialization
	void Start () {


		if (getAnimator == true) {
			anim = GetComponent<Animator> ();
		}

		rigid = GetComponent<Rigidbody2D>();

		speedLimit = GameObject.FindGameObjectWithTag ("SpeedLimit");
		speedLimitScript = speedLimit.GetComponent<SpeedLimit> ();

		playerContainer = GameObject.FindGameObjectWithTag ("PlayerContainer"); 
		script = playerContainer.GetComponent<PlayerContainer> ();

		player = GameObject.FindGameObjectWithTag ("Player"); 
		playerScript = playerContainer.GetComponent<PlayerMovement> ();

		randomBackground = GameObject.FindGameObjectWithTag ("RandomBackground");
		randomBackScript = randomBackground.GetComponent<GetBackground> ();

		checkCarBox = new Vector2 (0.5f, 0.5f);

		speed = speedIndex [1];

		if (transform.position.y > 0) {
		
			 speed*= -1;
		} 

			
	}

	// Update is called once per frame
	void Update ()

	{

		//Checkt ob ob das dreißiger Item eingesammelt wurde und verlangsamt die Autos
		if (speedLimitScript.slow == true) {


			if (transform.position.x < 0) {
				speed = -speedIndex [0];

			} else {

				Debug.Log ("YES");
				speed = speedIndex [0];

			}

		} else  {
		
			speed_Handler ();
		}

		rigid.velocity = new Vector2 (rigid.velocity.x, speed); // Gibt dem Car eine Bewegung
	}

	public void speed_Handler(){


		isCollided = Physics2D.OverlapBox (transform.GetChild(0).gameObject.transform.position, checkCarBox,2,CheckLayer);

		if (isCollided) {

			if (transform.position.x < 0) {

				speed = -4;

			} else {

				speed = 4;

			}


		} else {

			if (script.score <= 7 && speedLimitScript.slow == false) {

				//Checkt ob oben Gespawnt wird oder unten je nach dem wird der Speed negativ oder positiv genommen
				if (transform.position.x < 0) {

					speed = -speedIndex [1];

				} else {

					Debug.Log ("YES");
					speed = speedIndex [1];

				}
			}

			//// Autos werden schneller abhängig vom score
			if (script.score > 7 && speedLimitScript.slow == false) {

				//Checkt ob oben Gespawnt wird oder unten je nach dem wird der Speed negativ oder positiv genommen
				if (transform.position.x < 0) {

					speed = -speedIndex [2];

				} else {

					Debug.Log ("YES");
					speed = speedIndex [2];

				}
			}

			if (script.score >= 17 && speedLimitScript.slow == false) {
				//Checkt ob oben Gespawnt wird oder unten je nach dem wird der Speed negativ oder positiv genommen
				if (transform.position.x < 0) {

					speed = -speedIndex [3];

				} else {

					Debug.Log ("YES");
					speed = speedIndex [3];

				}


			}
		}


	}
		
	// Collisions Erkennung 
	void OnTriggerEnter2D (Collider2D coll){
	
		if (coll.gameObject.tag == "CarDestroy") {


			Destroy (gameObject);
		}


	}

	#if UNITY_EDITOR
	void OnDrawGizmos(){

		Gizmos.color = Color.blue;
		checkCarBox = new Vector2 (0.5f, 0.5f);
		isCollided = Physics2D.OverlapBox (transform.GetChild(0).gameObject.transform.position, checkCarBox,2,CheckLayer);
		Gizmos.DrawWireCube (transform.GetChild(0).gameObject.transform.position, checkCarBox);
	}
	#endif

}



		
	
		
	





	
	

