﻿using UnityEngine;
using System.Collections;

public class ShakingScreen : MonoBehaviour {

	public Camera mainCam;
	float shakeAmount = 0;

	void Awake(){
	
		if (mainCam == null) {
		
			mainCam = Camera.main;
		}
	}

	void Update(){

		if (Input.GetKeyDown (KeyCode.S)) {
		
			Shake (0.1f, 0.2f);
		}
	}

	public void Shake(float amount, float length){

		shakeAmount = amount;
		InvokeRepeating ("BeginShake",0,0.01f);
		InvokeRepeating ("StopShake", length,0);
	}

	void BeginShake(){

		if (shakeAmount > 0) {
		
			Vector3 camPos = mainCam.transform.position;
			float shakeAmtX = Random.value * shakeAmount * 2 - shakeAmount;
			float shakeAmtY = Random.value * shakeAmount * 2 - shakeAmount;
			camPos.x += shakeAmtX;
			camPos.y += shakeAmtY;

			mainCam.transform.position = camPos;
		}

	}

	void StopShake(){
	
		CancelInvoke ("BeginShake");
		mainCam.transform.position = new Vector3(0,0, transform.position.z);
	}

}
