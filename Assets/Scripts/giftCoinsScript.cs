﻿using UnityEngine;
using System.Collections;

public class giftCoinsScript : MonoBehaviour {

	public GameObject coinsCounter;
	public float timer = 2;
	public PlayerContainer pScript;

	// Use this for initialization
	void Start () {
	
		coinsCounter = GameObject.Find ("CoinAnzeige");
		pScript = GameObject.Find ("PlayerContainer").GetComponent<PlayerContainer>();
	
	}
	
	// Update is called once per frame
	void Update () {

		timer -= Time.deltaTime;

		if (timer < 1) {

			gameObject.GetComponent<RectTransform> ().localPosition = Vector2.Lerp (gameObject.GetComponent<RectTransform> ().localPosition, coinsCounter.GetComponent<RectTransform> ().localPosition, 0.1f);
		}


		if (Vector2.Distance (gameObject.GetComponent<RectTransform> ().localPosition, coinsCounter.GetComponent<RectTransform> ().localPosition) < 4) {
		
			Destroy (gameObject);
			pScript.coins++;

		}
	}


}
