﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnContainer : MonoBehaviour {

	public GameObject spawnPointPos;

	public void SetSpawnManagerPos(){
	

		spawnPointPos = GameObject.FindGameObjectWithTag ("MySpawnPos");

		for (int i = 0; i < transform.childCount; i++) {

			transform.GetChild (i).transform.position = spawnPointPos.transform.GetChild (i).transform.position;
		}
	
	}

}
