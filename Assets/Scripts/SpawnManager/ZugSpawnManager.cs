﻿using UnityEngine;
using System.Collections;

public class ZugSpawnManager : MonoBehaviour {

	[Header("Zug Array")]
	[Tooltip("Hier können mehrere Zugtypen eingesetzt werden wenn nötig")]
	public GameObject[] train;
	public GameObject[] horrorTrain;
	public GameObject[] apoTrain;
	public GameObject[] scifieTrain;
	[Header("Einbundung PlayerScript")]
	public GameObject playerContainer;
	public PlayerContainer script;

	[Header("Zug Spawn System")]
	[Tooltip("Ist dieser Timer gleich 0 Spawnt ein Zug")]
	public float trainSpawnTimer;
	[Tooltip("Setzt trainSpawnTimer wieder auf eine Random Zahl zurück")]
	public float randomTimeReset;
	[Tooltip("Diese Variable orientiert sich am Score. Ab welchen Score soll der Zug Spawnen?")]
	public float spawnNow; //Wann der Zug Spawnen soll am Score gerichtet
	[Tooltip("Soll die Ampel Schalten?")]
	public bool ampelSchalten = false; //sagt dem Ampel Script das es schalten soll
	[Tooltip("Wie lange soll die Ampel grün bleiben?")]
	public float ampelgruenTimer = 3;
	[Tooltip("Setzt ampelgruenTimer zurück")]
	public float ampelgruenTimerReset;
	[Tooltip("Erlaubt ampelgruenTimer zu starten")]
	public bool startGruenTimer = false;


	// Use this for initialization
	void Start () {
		
		playerContainer = GameObject.FindGameObjectWithTag ("PlayerContainer");
		script = playerContainer.GetComponent<PlayerContainer>(); // ändern zu PlayerContainer


	}
	
	// Update is called once per frame
	void Update () {
	
		if (script.score > spawnNow) {
		
			trainSpawnTimer -= Time.deltaTime;

			//Timer bestimmt wann zug gespawnt werden soll
			if (0 > trainSpawnTimer) {



				if (PlayerContainer.selectionIndex == 0 || PlayerContainer.selectionIndex == 1)
                { 
                    Instantiate(train[0], transform.position, Quaternion.identity);
                }
				if (PlayerContainer.selectionIndex == 2)
                {
                    Instantiate(horrorTrain[0], transform.position, Quaternion.identity);
                }

				if (PlayerContainer.selectionIndex == 3)
				{
					Instantiate(apoTrain[0], transform.position, Quaternion.identity);
				}

				if (PlayerContainer.selectionIndex == 4)
				{
					Instantiate(scifieTrain[0], transform.position, Quaternion.identity);
				}


				//fürs anpassen von verschiedenen Zügen zu den Backgrounds//Arrays erweitern //Einbinden von GteBackgroundScript// switch!

                ampelSchalten = true;
				randomTimeReset = Random.Range (5, 10);
				trainSpawnTimer = randomTimeReset;
				startGruenTimer = true;


			}
				
		}

		//Timer bestimmt wann ampel wieder grün werden soll
		if (startGruenTimer == true) {
			ampelgruenTimer -= Time.deltaTime;

			transform.GetChild (0).gameObject.SetActive (true);

			if (0 > ampelgruenTimer) {

				ampelSchalten = false; //Diese variable bestimmt ob die ampel schaltet oder nicht
				startGruenTimer = false;
				ampelgruenTimer = ampelgruenTimerReset; //bestimmt wann die ampel wieder zrucükschalten soll
				transform.GetChild (0).gameObject.SetActive (false);
			}
		}


	}
}
