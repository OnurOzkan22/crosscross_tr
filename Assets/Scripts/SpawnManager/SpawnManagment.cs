﻿using UnityEngine;
using System.Collections;

public class SpawnManagment : MonoBehaviour {

	[Header("Spawn System Cars")]
	[Tooltip("Array für GameObject kann beliebig erweitert werden")]
	public GameObject[] cars;
	public GameObject[] horror;
	public GameObject[] apocalypitca;
	public GameObject[] sciFi;
	[Tooltip("Bestimmt Random ein Auto")]
	public int random;
	[Tooltip("Startet Das Spawnen wieder nach dem die 30iger Zone wieder deaktiviert ist")]
	//public bool startSM = true;
	[Header("Arrow anzeigen wenn Auto Spawnt")]
	public GameObject arrowAttention;
	public GameObject spawnPointPos;
	public GameObject currentCar;
	public float dist;

	void Awake (){

		gameObject.SetActive (false);
	}

	// Use this for initialization
	void Start () {

		random = Random.Range (0, 5); //generiert eine random Zahl

		//Checkt welche Hintergrund aktiev ist und Spawnt je nach dem Autos aus dem zugewissenen Array
		if (PlayerContainer.selectionIndex == 0 || PlayerContainer.selectionIndex == 1) {

			currentCar = (GameObject)Instantiate (cars [random], transform.position, Quaternion.identity); //Spawnt das Object

		} else if (PlayerContainer.selectionIndex == 2) {

			currentCar = (GameObject)Instantiate (horror [random], transform.position, Quaternion.identity); //Spawnt das Object

		} else if (PlayerContainer.selectionIndex == 3) {

			currentCar = (GameObject)Instantiate (apocalypitca [random], transform.position, Quaternion.identity);

		} else if (PlayerContainer.selectionIndex == 4) {

			currentCar = (GameObject)Instantiate (sciFi [random], transform.position, Quaternion.identity);
		}

	}
	
	// Update is called once per frame
	void Update ()
	{
		dist = Vector2.Distance (transform.position, currentCar.transform.position);

		if (Vector2.Distance(transform.position, currentCar.transform.position) > Mathf.Abs(currentCar.transform.localScale.y * 2) + 5) {
			

			random = Random.Range (0, 5); //generiert eine random Zahl

			//Checkt welche Hintergrund aktiev ist und Spawnt je nach dem Autos aus dem zugewissenen Array
			if (PlayerContainer.selectionIndex == 0 || PlayerContainer.selectionIndex == 1) {
				
				currentCar = (GameObject)Instantiate (cars [random], transform.position, Quaternion.identity); //Spawnt das Object

			} else if (PlayerContainer.selectionIndex == 2) {
				
				currentCar = (GameObject)Instantiate (horror [random], transform.position, Quaternion.identity); //Spawnt das Object

			} else if (PlayerContainer.selectionIndex == 3) {
				
				currentCar = (GameObject)Instantiate (apocalypitca [random], transform.position, Quaternion.identity);

			} else if (PlayerContainer.selectionIndex == 4) {

				 currentCar = (GameObject)Instantiate (sciFi [random], transform.position, Quaternion.identity);
			}
				
		}
			

	}
		
}

