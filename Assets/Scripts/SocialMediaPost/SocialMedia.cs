﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SocialMedia : MonoBehaviour {

	public Text twitterText;
	public Text facebookText;
	public string newTwitterText;
	public string newFacebookText;

	public GameObject playerContainer;
	public int howManyCoinsAdd;
	public int twitterLike = 0;
	public int facebookLike = 0;


	// Use this for initialization
	void Start () {
	
		twitterLike = PlayerPrefs.GetInt ("twitter");
		PlayerPrefs.GetString ("SaveTwitterText");

		facebookLike = PlayerPrefs.GetInt ("facebook");
		PlayerPrefs.GetString ("SaveFacebookText");

		if (twitterLike == 0) {
		
			twitterText.text = "Get +" + howManyCoinsAdd + "C";

		} else {
		
			twitterText.text = newTwitterText;
		}

		if (facebookLike == 0) {

			facebookText.text = "Get +" + howManyCoinsAdd + "C";

		} else {

			facebookText.text = newFacebookText;
		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//crongagames Twitter
	public void twitterChannel(){

		Application.OpenURL ("https://twitter.com/tinyreapergames");

		if (twitterLike == 0) {
		
			twitterLike = 1;
			twitterText.text = newTwitterText;
			playerContainer.GetComponent<PlayerContainer> ().coins += howManyCoinsAdd;
			PlayerPrefs.SetInt ("Coins", playerContainer.GetComponent<PlayerContainer> ().coins);
			PlayerPrefs.SetInt ("twitter", twitterLike);

		}
	}

	public void FaceBookChannel(){

		Application.OpenURL ("https://www.facebook.com/tinyreapergames/");

		if (facebookLike == 0) {

			facebookLike = 1;
			facebookText.text = newFacebookText;
			playerContainer.GetComponent<PlayerContainer> ().coins += howManyCoinsAdd;
			PlayerPrefs.SetInt ("Coins", playerContainer.GetComponent<PlayerContainer> ().coins);
			PlayerPrefs.SetInt ("facebook", facebookLike);

		}
	}
}

