﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class HomeButton : MonoBehaviour {

	public GameObject playerContainer;
	public PlayerContainer script;

	[Header("Einbindung StartButton")]
	public GameObject startButton;
	public StartButton startScript;


	[Header("GP Leaderboard ID")]
	public string leaderboard;

	void Start(){

		playerContainer = GameObject.FindGameObjectWithTag ("PlayerContainer");
		script = playerContainer.GetComponent<PlayerContainer> (); 

		startButton = GameObject.FindGameObjectWithTag ("StartButton");
		startScript = startButton.GetComponent<StartButton> ();

	}

	public void OnClick(){

		startScript.directRestart = 0;
		PlayerPrefs.SetFloat ("directRestart", startScript.directRestart);

		if (Social.localUser.authenticated) {
			Social.ReportScore (script.score, leaderboard, (bool success) => {


			});

		}


		SceneManager.LoadScene ("Scene1");


	}



}


