﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class GameOverScreenAdGift : MonoBehaviour {

	private List<GameObject> models;
	private int selectionIndex;

	// Use this for initialization
	void Start () {
	
		models = new List<GameObject> ();

		foreach (Transform t in transform) {
		
			models.Add (t.gameObject);
			t.gameObject.SetActive (false);
		}
			
		if (Random.value > 0.7F) {
		
			SelectItem (1);

		} else {
		
			SelectItem (0);
		}

		models [selectionIndex].SetActive (true);
	}
	
	public void SelectItem (int index){
	
		if (index == selectionIndex) {
		
			return;
		}

		if (index < 0 || index >= models.Count) {
		
			return;
		}

		models [selectionIndex].SetActive (false);
		selectionIndex = index;
		models [selectionIndex].SetActive (true);

	}
}
