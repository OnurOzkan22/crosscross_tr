﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class RestartButton : MonoBehaviour {

	public GameObject playerContainer;
	public PlayerContainer script;

	[Header("Einbindung StartButton")]
	public GameObject startButton;
	public StartButton startScript;


	[Header("GP Leaderboard ID")]
	public string leaderboard = "CgkIv9W-7pMCEAIQAA";

	void Start(){

		playerContainer = GameObject.FindGameObjectWithTag ("PlayerContainer");
		script = playerContainer.GetComponent<PlayerContainer> (); 

		startButton = GameObject.FindGameObjectWithTag ("StartButton");
		startScript = startButton.GetComponent<StartButton> ();
	}

	public void directStart(){

		startScript.directRestart = 1;
		PlayerPrefs.SetFloat ("directRestart", startScript.directRestart);

		if (Social.localUser.authenticated) {

			// post score 12345 to leaderboard ID "Cfji293fjsie_QA")
			Social.ReportScore(12345, "Cfji293fjsie_QA", (bool success) => {
				// handle success or failure
			});
		}



		SceneManager.LoadScene ("Scene1");

	}



}
	

