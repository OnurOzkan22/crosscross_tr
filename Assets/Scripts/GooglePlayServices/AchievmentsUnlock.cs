﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

//CanTouchThis findest du im PlayerContainer Script!

public class AchievmentsUnlock : MonoBehaviour {

	public string[] achievements;
	public GameObject playerContainer;
	public PlayerContainer script;


	// Use this for initialization
	void Start () {
	
		playerContainer = GameObject.FindGameObjectWithTag ("PlayerContainer");
		script = playerContainer.GetComponent<PlayerContainer> ();


	}
	
	// Update is called once per frame
	void Update () {
	
		///Score
		if (Social.localUser.authenticated) {

			/// Train Arriving
			if (script.score > 10) {
		
				Social.ReportProgress (achievements [0], 100.0f, (bool success) => {
				
				});
			}

			/// Fast Food 80
			if (script.score > 7) {

				Social.ReportProgress (achievements [1], 100.0f, (bool success) => {

				});
			}

			/// Autobahn
			if (script.score > 17) {

				Social.ReportProgress (achievements [2], 100.0f, (bool success) => {

				});
			}

		///Score

		//Coins
			/// Another day another diem
			if (script.coins > 0) {

				Social.ReportProgress (achievements [3], 100.0f, (bool success) => {

				});
			}

			///Money Boy
			if (script.coins > 9) {

				Social.ReportProgress (achievements [4], 100.0f, (bool success) => {

				});
			}

			///Rich Bich
			if (script.coins > 19) {

				Social.ReportProgress (achievements [5], 100.0f, (bool success) => {

				});
			}

			///Gold Digger
			if (script.coins > 49) {

				Social.ReportProgress (achievements [6], 100.0f, (bool success) => {

				});
			}
		//Coins

		//DeathCouner
			///First Dead
			if(script.tenDeath > 0){

				Social.ReportProgress (achievements [7], 100.0f, (bool success) => {

				});
			}

			///Suicidal
			if(script.tenDeath > 9){

				Social.ReportProgress (achievements [8], 100.0f, (bool success) => {

				});
			}

		}
	}
}
