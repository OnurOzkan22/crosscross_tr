﻿using UnityEngine;
using System.Collections;


public class StatsButton : MonoBehaviour {

	public Animator animStatsPanel;
	public GameObject statsPanel;
	//public AudioSource buttonSound;

	// Use this for initialization
	void Start () {
	
		animStatsPanel = statsPanel.GetComponent<Animator> ();
		//buttonSound = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnClickStats(){

		animStatsPanel.Play ("StatsPanelIn");
		//buttonSound.Play ();

	}

	public void OnClickExit(){

		animStatsPanel.Play ("StatsPanelOut");
		//buttonSound.Play ();
	}
}
