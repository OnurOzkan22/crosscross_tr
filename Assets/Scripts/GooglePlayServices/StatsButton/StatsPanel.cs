﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatsPanel : MonoBehaviour {

	public int deaths;
	public int scorePoints;
	public int collectedCoins;
	public int runsCounter;

	public Text[] statTexts;


	// Use this for initialization
	void Start () {

		deaths = PlayerPrefs.GetInt ("deaths");
		scorePoints = PlayerPrefs.GetInt ("scorePoints");
		collectedCoins = PlayerPrefs.GetInt ("coinsCollected");
		runsCounter = PlayerPrefs.GetInt ("runs");

		statTexts[0].text = string.Format ("Score Points: {0}", scorePoints);
		statTexts[1].text = string.Format ("Deaths: {0}", deaths);
		statTexts [2].text = string.Format ("Coins: {0}", collectedCoins);
		statTexts [3].text = string.Format ("Runs: {0}", runsCounter);
	}
	
	// Update is called once per frame
	void Update () {
	


	}
}
