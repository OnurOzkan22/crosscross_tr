﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class ShowAchievement : MonoBehaviour {


	public void callAchievements ()
	{

		if (Social.localUser.authenticated) {
			Social.ShowAchievementsUI ();
		} else {

			PlayGamesPlatform.Activate ();
			Social.localUser.Authenticate ((bool success) => {

			});
		}
	}
}
