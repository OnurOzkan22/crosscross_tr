﻿using UnityEngine;
using System.Collections;

public class CoinsSpawnManager : MonoBehaviour {

	public GameObject[] dollars;
	public float timer;
	public float timerReset;

	// Use this for initialization
	void Start () {
	

	}
	
	// Update is called once per frame
	void Update () {
	
		timer -= Time.deltaTime;

		if (timer <= 0) {
		
			float randomGoldDollar = Random.value;
			float randomXPos = Random.Range (-2.5f, 3.5f);

			if (randomGoldDollar > 0.9f) {
			
				Instantiate (dollars [0], transform.position, Quaternion.identity);
				timerReset = Random.Range (3f, 5);
				timer = timerReset;
				transform.position = new Vector2 (randomXPos, transform.position.y);

			} else if(randomGoldDollar < 0.7f){
				
				Instantiate (dollars [1], transform.position, Quaternion.identity);
				timerReset = Random.Range (3f, 5);
				timer = timerReset;
				transform.position = new Vector2 (randomXPos, transform.position.y);
			}
		}
	}
}
