﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpeedLimit : MonoBehaviour {

	
	[Header("Audio")]
	public AudioSource sound;

	[Header("Geschwindigkeitseinstellungen")]
	[Tooltip("Bestimmt wie lange die 30iger Zone aktiv sein soll")]
	public float timer = 5; // Startet 30iger Zone
	[Tooltip("Erlaubt Timer zu starten")]
	public bool timerStart = false;
	[Tooltip("Setzt Timer auf alten Wert zurück")]
	public float timerReset = 5;
	[Tooltip("Wenn true dann haben alle Autos einen SpeedIndex von 0 (siehe CarMovement Script)")]
	public bool slow = false;
	[Tooltip("Legt eine random X Position fest für das Spawnen des 30iger Items")]
	public float randomPos; // Bestimmt eine Random Pos für das Schild
	[Tooltip("Hier Text einfügen für SpeedLimit")]
	public Text speedLimitText;
	public GameObject speedLimit;


	[Header("30iger Item Spawn System")]
	[Tooltip("Bestimmt wie lange das Schild sichtbar sein soll beim Spawn")]
	public float timerSchildSpawn = 5; //Wie lange das Schild gespawnt sein soll
	[Tooltip("Setzt timerSchildSpawn auf alten Wert zurück")]
	public float timerSchildReset = 5;
	[Tooltip("Wenn true dann fängt timerSchildSpawn an runter zu zählen ")]
	public bool SpawntimerStart = true;
	[Tooltip("Wie lange soll das 30iger Item nicht Spawnen ")]
	public float waitTime = 3; // Wie lang das Schild nicht im Spiel sein soll
	[Tooltip("Setzt wait Time auf alten Wert zurück ")]
	public float ResetWaitTime = 3;
	[Tooltip("Wenn true aktiviert sich die Animation für die 30iger Zone")]
	public bool dreiSchild = false; //Aktiviert die Anim für 30iger Schild

	[Header("Eindungen der SpawnManager")]
	public GameObject spawnManager1Ul;
	public GameObject spawnManager2Ur;
	public GameObject spawnManager3Dl;
	public GameObject spawnManager4Dr;
	[Header("Eindungen der SpawnManagerScripte")]
	public SpawnManagment smscript1;
	public SpawnManagment smscript2;
	public SpawnManagment smscript3;
	public SpawnManagment smscript4;

	GameObject spawnContainer;

	public string dreisiger;

	// Use this for initialization
	void Start () {
	

		slow = false;
		//speedLimitText.enabled = false

		sound = GetComponent<AudioSource> ();

		//Einbinden von SpawnManager
		spawnManager1Ul = GameObject.FindGameObjectWithTag ("SpawnUL");
		smscript1 = spawnManager1Ul.GetComponent<SpawnManagment> ();
		spawnManager2Ur = GameObject.FindGameObjectWithTag ("SpawnUR");
		smscript2 = spawnManager2Ur.GetComponent<SpawnManagment> ();

		spawnManager3Dl = GameObject.FindGameObjectWithTag ("SpawnDL");
		smscript3 = spawnManager3Dl.GetComponent<SpawnManagment> ();
		spawnManager4Dr = GameObject.FindGameObjectWithTag ("SpawnDR");
		smscript4 = spawnManager4Dr.GetComponent<SpawnManagment> ();
		//////


		transform.position = new Vector2 (transform.position.x, 10); //Spawnt 30iger außerhalb damit nicht eingesammelt werden kann

		SpeedAnzeige speedAnzeige = GameObject.Find ("Speedanzeige").GetComponent<SpeedAnzeige> ();
		speedAnzeige.Find ();
	}
	
	// Update is called once per frame
	void Update () {
	
		//////Schild Spawnt und verschwindet
		if (SpawntimerStart) {
		
			timerSchildSpawn -= Time.deltaTime;
		}


		if (0 > timerSchildSpawn) {
		

			timerSchildSpawn = timerSchildReset;
		} 

		if (timerSchildSpawn == timerSchildReset) {

			SpawntimerStart = false;
			waitTime -= Time.deltaTime;
		}

		if (0 < waitTime && SpawntimerStart == false) { 

			Vector2 newPos = new Vector2 (20, 10f);
			transform.position = newPos;

		} else if (0 > waitTime) {

			SpawntimerStart = true;
			waitTime = ResetWaitTime;
			randomPos = Random.Range (2.90f, -2.50f);
			Vector2 newPos = new Vector2 (randomPos, -1.1f);
			transform.position = newPos;
		}

		///////////


		////Aktiviert 30iger Zone
		if (timerStart) {
			speedLimit.SetActive (true);
			speedLimitText.text = "" + Mathf.Round(timer);
			timer -= Time.deltaTime;
			////Hier spawnt der Spawn Manager wieder alle Autos wenn 30iger vorbei ist
			if (0 > timer) {

				dreiSchild = false;
				timer = timerReset;
				slow = false;
				speedLimit.SetActive (false);
				timerStart = false;
			}
				
		}
	}


	////Hier soll er solanger 30iger aktiv ist keine Autos spawnen
	void OnTriggerEnter2D (Collider2D coll){
	
		if (coll.gameObject.tag == "Player") {
		
			sound.Play ();
			Vector2 newPos = new Vector2 (20, 10f);
			transform.position = newPos;
			slow = true;
			dreiSchild = true;
			timerStart = true;

			if (Social.localUser.authenticated) {
				Social.ReportProgress (dreisiger, 100.0f, (bool success) => {

				});
			}
		}
			//transform.position = new Vector2 (transform.position.x, 10);

		}

	}