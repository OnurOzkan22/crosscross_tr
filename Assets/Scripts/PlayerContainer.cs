﻿/*Wie erstellst du ein neuen Player

1. Füge dem Player Container einen neuen Player hinzu als ChildObject
2. Weise dem Player ein PlayerMovement Script hinzu
3. Erweitere den Code im SaveSelectedPlayer Script und füge im Array das Image vom Neuen player ein das im Charakter Auswahl menu angezeigt wird
4. Erstell ein neuen Button mit dem SelectPlayer Script und füge die entsprechenden Array Werte ein
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class PlayerContainer : MonoBehaviour {

	[Header("Stop MainMusic bei GameOver")]
	public AudioSource music;
	public AudioSource gameOverSound;
	[Header("Stats counter")]
	public GameObject statsCounter;
	[Header("Einbindung Player")]
	public GameObject player;
	public PlayerMovement playerScript;

	[Header("Einbindung StartButton")]
	public GameObject startButton;
	public StartButton startScript;

	[Header("Text für Coins Anzeige")]
	[Tooltip("Hier Text einfügen der den Coins Text anzeigen soll im Spiel")]
	public Text coinsText;
	public int coins;
	public int coinsCollected;
	[Header("Text für GameOver Anzeige")]
	[Tooltip("Hier Text einfügen der den GameOver Text anzeigen soll im Spiel")]
	public Text gameOverText;
	[Header("Text für Highscore Anzeige")]
	[Tooltip("Hier Text einfügen der den Your Score Text anzeigen soll im Spiel")]
	public Text yourScoreText;
	[Tooltip("Hier Background von GameOverScreen einfügen")]
	public GameObject gameOverBackground;
	[Header("Score Anzeige")]
	public int score = 0;
	public static int scoreForAlert;
	[Header("Text für Scoreanzeige")]
	[Tooltip("Hier Text einfügen der den Score anzeigen soll im Spiel")]
	public Text scoreText;
	/*[Tooltip("Hier Text einfügen der den Highscore anzeigen soll im Spiel")]
	public Text highscoreText;*/
	[Header("Timer")]
	[Tooltip("Hier Text einfügen der den Timer anzeigen soll im Spiel")]
	public Text gameTimerText;
	[Tooltip("Hier einen Wert für den Timer eingeben")]
	public float gameTimer = 45;
	[Header("Wenn True startet timer")]
	public bool startTimer = false;
	[Header("Extra Life Item")]
	[Tooltip("Wenn Spieler ein extra Leben gekauft hat")]
	public int playerLife = 0;
	[Tooltip("Text für extra life")]
	public Text extraLifeText;
	public int deathCounter = 0;
	[Header("Plus 5Sekunden Anzeige")]
	public GameObject plusSec;
	[Header("StopSpawn Timer")]
	public float stopSpawnTimer;
	[Header("Achievements")]
	public float tenDeath; // zählt die tode
	public Animator anim;
	[Header("Zeigt deine letzte DeathPos an")]
	public GameObject deathPos;
	public float playerXpos;
	[Header("Play Death Anim (in PlayerMovementScript")]
	public bool playDeathAnim;
	[Header("SpeedAlert")]
	public GameObject speedAlert;


	//Charakter Auswahl system
	public static List<GameObject> models;
	public static int selectionIndex = 0; // Default Index of the Player

	[Header("Player Options")]
	public float playerSpeed;
	Rigidbody2D rigid;

	[Header("Tutorial")]
	public float waitFotTutTimer;
	public Text timerText;

	[Header("PlayerIsComing Image")]
	public GameObject playerIsComing;
	[Header("PlayerIsComing Image")]
	public GameObject restartButton;
	public GameObject homeButton;

	public bool getHighscore = false;
	[Header("Hier neuen LevelText für neuen Char einfügen!")]
	public Text[] levelText;
	public Image[] bar;

	public static bool getXPosOfPanel;
	public static bool ready = false;
	public bool isGameOver = false;



	// Use this for initialization
	void Start () {

		#region code in Start
		//Saved und plaziert GameObject an der letzten DeatPos
		playerXpos = PlayerPrefs.GetFloat ("savePos");
		deathPos.transform.position = new Vector2 (playerXpos, transform.position.y);

		//Animator für Warnsignal wenn timer bei !0sek ankommt
		anim = gameTimerText.GetComponent<Animator> ();
		anim.enabled = false;

		//Save tenDeath Achievement
		tenDeath = PlayerPrefs.GetFloat("10Death");

		//SaveCoins State
		coins = PlayerPrefs.GetInt ("Coins");



		rigid = GetComponent<Rigidbody2D> ();

		startButton = GameObject.FindGameObjectWithTag ("StartButton");
		startScript = startButton.GetComponent<StartButton> ();


		//////////////////////////////////////////////////////////////////
		/// Beim starten der Funktion wird "models" eine Liste zugewissen
		models = new List <GameObject> ();

		//Jedes Child Object wird in die Liste "models" gepackt
		foreach (Transform t in transform) {
		

			models.Add (t.gameObject); //wird in die Liste "models" gepackt
			t.gameObject.SetActive (false); //deaktiviert alle Childs

		}

		selectionIndex = PlayerPrefs.GetInt ("SavePlayer");
		AudioController.GetThisThemeOnStart(selectionIndex);
		models [selectionIndex].SetActive (true); //aktiviert das Default (Also erstes Object in der List)
		/////////////////////////////////////////////////////////////////////

		SaveSelectedPalyer savePos = GameObject.Find("ButtonPanel").GetComponent<SaveSelectedPalyer>();
		savePos.GetFirstStartPos();
		savePos.MarkThisPlayer();

		#endregion
		//Aktualisiert die Level texte für jeden Player im Menu
		for (int i = 0; i < transform.childCount; i++) {
		
			levelText[i].text = "Level:" + PlayerPrefs.GetInt(transform.GetChild(i).name + "level");
			bar[i].fillAmount = PlayerPrefs.GetFloat (transform.GetChild(i).name + "bar");
		}

		FindCurrentPlayer ();

	}

	public void FindCurrentPlayer(){

		player = GameObject.FindGameObjectWithTag ("Player");
		playerScript = player.GetComponent<PlayerMovement> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		#region code in Update


		if (isGameOver == false) {
			
			rigid.velocity = new Vector2 (playerSpeed, rigid.velocity.y);
		}

		playerXpos = transform.position.x;

		//Timer wenn StopSpawn Item aktiviert wurde
		if (startScript.stopStart == true && startScript.startTimerStopSpawn == true) {

			stopSpawnTimer -= Time.deltaTime;
			//Warte bis stopSpawnTimer kleiner 1 ist dann Aktiviere diese Vehicle
			if (1 > stopSpawnTimer) {
			
				startScript.objects [0].SetActive (true);//SpwanManager
				startScript.objects [1].SetActive (true);//SpwanManager
				startScript.objects [2].SetActive (true);//SpwanManager
				startScript.objects [3].SetActive (true);//SpwanManager
				startScript.objects [4].SetActive (true); //30iger Item
				startScript.objects [11].SetActive (true); //30iger Schild
				startScript.objects [0].GetComponentInParent<SpawnContainer> ().SetSpawnManagerPos ();
				stopSpawnTimer = 0;
			}
		}

		//extraLife Text
		extraLifeText.text = "" + playerLife;

		//CoinsText
		coinsText.text = "" + coins;

		//ScoreText
		scoreText.text = "" + score;

		//YourScoretext nach GameOver anzeige
		yourScoreText.text = "" + score;

		if (ready) {

			foreach (Touch touch in Input.touches) {

				if (touch.phase == TouchPhase.Began) {

					startTimer = true;
					startScript.objects [6].SetActive (false);
					startScript.objects [14].SetActive (false);
					ready = false;
				}
			}
		}

			///Game Timer + Text + STARTET DEN RUN!
			if (startTimer == true) {
			
				gameTimer -= Time.deltaTime;
				gameTimerText.text = Mathf.Round (gameTimer) + ""; //macht daraus eine nicht Fließkommerzahl
			 
		
				//färbt ab 10sekunden rot ein als Warnsignal
				if (10 > gameTimer) {

					gameTimerText.color = Color.red;
					anim.enabled = true;
		
				} else {

					gameTimerText.color = Color.white;
					anim.enabled = false;
				}	
				////////////////////////////////////////////

				//Wenn GameTimer Null ist GameOver
				if (1 > gameTimer) {

					GameOver ();
					gameTimer = 0;
					playerSpeed = 0;
					PlayerPrefs.SetInt ("Coins", coins); ////SAVE COINS STATS
				}
				//////////////////////////////////
		
			}

			#endregion

	}
	////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////////
	/// Funktion zur Auswahl von einem Object aus der Liste (ChildObject)
	public void Select (int index){

		//Wenn Index gleich selectionIndex ist dann gib den wert zurück
		if (index == selectionIndex) {
		
			return;
		}

		//Wenn Index kleiner oder wenn Index größer/gleich der Anzahl der Liste ist gib den Wert zurück
		if (index < 0 || index >= models.Count) {

			return;
		}

		models [selectionIndex].SetActive (false);
		selectionIndex = index;
		models [selectionIndex].SetActive (true);
		PlayerPrefs.SetInt ("SavePlayer", selectionIndex);

	}
	/////////////////////////////////////////////////////////////////////

	void OnTriggerEnter2D (Collider2D coll){
		if (coll.gameObject.tag == "Counter") {

			transform.position = new Vector2 (9.14f, -0.97f);
			playerScript.AddLevelPoint ();
			score++;
			gameTimer += 5;//Wie viel sekunden sollen addiert werden auf den Timer
			plusSec.SetActive (true);
			playerIsComing.SetActive (true);
			StartCoroutine ("PlusTextEnabled");
			transform.GetComponentInChildren<PlayerMovement> ().TurnOnAllStopPointsCollider ();

		}

	
	}
		
	//Zeigt Plus 5Sek an beim collidieren mit Counter
	IEnumerator PlusTextEnabled (){

		yield return new WaitForSeconds (1.5f);
		plusSec.SetActive (false);
		StopCoroutine ("PlusTextEnabled");
	}

	//Zeigt Go zeichen an beim start
	IEnumerator GO(){
		timerText.text = "GO!";
		yield return new WaitForSeconds(2);
		timerText.enabled = false;
		StopCoroutine ("GO");


	}

	//Game Over Funktion: Löst Game Over Screen aus
	public void GameOver (){

		rigid.Sleep ();
		isGameOver = true;
		PlayerPrefs.SetInt ("Coins", coins); ////SAVE COINS STATE
		GameObject.Find("Canvas").GetComponent<GraphicRaycaster> ().enabled = true; // erlaubt wieder mit dem UI zu interagieren
		AudioController.StopTheme();
		gameOverSound.Play ();
		getHighscore = true;
		rigid.isKinematic = true;
		startScript.startPlayerContainer = false;
		startTimer = false;
		PlayerMovement.touchStart = false;
		restartButton.SetActive (true);
		homeButton.SetActive (true);
		tenDeath++;
		PlayerPrefs.SetFloat ("10Death", tenDeath); //Achievment tentime death
		PlayerPrefs.SetFloat("savePos", playerXpos); //Save letzte deathPos
		Statistics();
		gameTimer = 0;
		startScript.objects [4].SetActive (false); //30iger Item wird deaktiviert
		startScript.objects [9].SetActive (false); //Speed Anzeige wird deaktiviert
		startScript.objects [5].SetActive (false); //CoinsManager wird deaktiviert
		startScript.objects [0].SetActive (false);//SpwanManager
		startScript.objects [1].SetActive (false);//SpwanManager
		startScript.objects [2].SetActive (false);//SpwanManager
		startScript.objects [3].SetActive (false);//SpwanManager
		startScript.objects [10].SetActive (false);//ZugSpwanManager
		startScript.objects [11].SetActive (false);//ZugSpwanManager
		gameOverBackground.SetActive (true);
		gameTimerText.enabled = false;
		scoreText.enabled = false;
		startScript.startPlayerContainer = false;

	}

	public void Statistics(){

		statsCounter.GetComponent<StatsPanel> ().deaths++; // FÜr die Statsistik
		statsCounter.GetComponent<StatsPanel> ().scorePoints += score; // FÜr die Statsistik
		statsCounter.GetComponent<StatsPanel> ().collectedCoins += coinsCollected;// FÜr die Statsistik
		PlayerPrefs.SetInt("deaths",statsCounter.GetComponent<StatsPanel> ().deaths);
		PlayerPrefs.SetInt("scorePoints",statsCounter.GetComponent<StatsPanel> ().scorePoints);
		PlayerPrefs.SetInt("coinsCollected",statsCounter.GetComponent<StatsPanel> ().collectedCoins);
		PlayerPrefs.SetInt("runs",statsCounter.GetComponent<StatsPanel> ().runsCounter + 1);
	}
		
}
