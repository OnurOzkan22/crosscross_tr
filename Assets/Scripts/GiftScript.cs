﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

//Im GiftObject childs einfügebn für gewünschte freischaltbare Chars Gifts
//Array erweitern!

public class GiftScript : MonoBehaviour {

	public GameObject playerContainer;
	public PlayerContainer script;
	public GameObject howManyCoins;
	private Image giftImage;
	public Text howManyCoinsText;
	private Button giftButton;
	private ulong lastGiftOpen;
	public float msToWait;
	public Text timerText;
	public GameObject buttons;
	private List<GameObject> models;
	private int selectionIndex;
	public int randomIndex;
	public GameObject giftCoins;
	public RectTransform rect;
	public GameObject can;
	public Animator anim;
	public bool openMe;


	// Use this for initialization
	void Start () {

		anim = GetComponent<Animator> ();

		can = GameObject.Find ("Canvas");

		rect = GetComponent<RectTransform> ();

		playerContainer = GameObject.FindGameObjectWithTag ("PlayerContainer");
		script = playerContainer.GetComponent<PlayerContainer> ();

		giftButton = GetComponent<Button> ();
		giftImage = GetComponent<Image> ();
	

		models = new List<GameObject>();

		foreach (Transform t in transform) {
		
			models.Add (t.gameObject);
			t.gameObject.SetActive (false);
		}

		//models [selectionIndex].SetActive (false);

		//lastGiftOpen = ulong.TryParse(PlayerPrefs.GetString ("LastTimer"));
		bool tryP = ulong.TryParse (PlayerPrefs.GetString ("LastTimer"), out lastGiftOpen);

		if (!isGiftReady ()) {

			giftButton.interactable = false;
		}


	}

	// Update is called once per frame
	void Update () {

		anim.SetBool ("openMe", openMe);

		if (!giftButton.IsInteractable ()) {
		
			if (isGiftReady ()) {
			
				giftButton.interactable = true;
				return;
			}

			//Set the timer
			ulong diff = ((ulong)DateTime.Now.Ticks - lastGiftOpen);
			ulong m = diff / TimeSpan.TicksPerMillisecond;
			float secondsLeft = (float)(msToWait - m) / 1000f;
			string r = "";

			//Hours
			r += ((int)secondsLeft / 3600).ToString() + ":";
			secondsLeft -= ((int)secondsLeft / 3600) * 3600;
			//Minutes
			r+= ((int)secondsLeft/60).ToString("00") + ":";
			//Seconds
			r+= (secondsLeft % 60).ToString("00") + "";
			timerText.text = r;

		}
			

	}


	public void SelectPlayerGift(int index){
	
		if (index == selectionIndex) {
		
			return;
		}

		if (index < 0 || index >= models.Count) {
		
			return;
		}

		models [selectionIndex].SetActive (false);
		selectionIndex = index;
		models [selectionIndex].SetActive (true);

	}

	public void PlayOpenAnim(){
	
		openMe = true;
	}

	//Gift wird geöffnet
	public void GetGift() {
	
		//giftImage.enabled = false;

		randomIndex = UnityEngine.Random.Range (1, 3); // hier erweitern für mehr Chars die freigeschaltet werden sollen & in GiftButton(GameObject) die Images einfügen

		//Wenn nicht schon freigeschaltet dann mach das
		if (buttons.transform.GetChild (randomIndex).GetComponent<SelectPlayer> ().isSelectedSave == 0 && UnityEngine.Random.value > 0.7f) {

				giftImage.enabled = false;
				SelectPlayerGift (randomIndex);
				buttons.transform.GetChild (randomIndex).GetComponent<SelectPlayer> ().isSelectedSave = 1;
				PlayerPrefs.SetFloat (buttons.transform.GetChild (randomIndex).GetComponent<SelectPlayer> ().gameObject.name, buttons.transform.GetChild (randomIndex).GetComponent<SelectPlayer> ().isSelectedSave);
				

			} else {
			//Wenn freigeschaltet mach das
		
				giftImage.enabled = false;
				int randomCoins = UnityEngine.Random.Range (20, 50);
				howManyCoins.SetActive (true);
				howManyCoinsText.text = "+" + randomCoins;
				//script.coins += randomCoins;
				PlayerPrefs.SetInt ("Coins", script.coins + randomCoins);

			for (int i = 0; i < randomCoins; i++) {

				GameObject currentCoin = (GameObject)Instantiate (giftCoins,rect.localPosition, Quaternion.identity, can.transform);
				currentCoin.GetComponent<RectTransform> ().localPosition = new Vector2 (UnityEngine.Random.Range(-300,300), UnityEngine.Random.Range(-300,300));
			}

			}

		lastGiftOpen = (ulong)DateTime.Now.Ticks;
		PlayerPrefs.SetString ("LastTimer", lastGiftOpen.ToString ());
		giftButton.interactable = false;

}
	

	private bool isGiftReady(){
	
		ulong diff = ((ulong)DateTime.Now.Ticks - lastGiftOpen);
		ulong m = diff / TimeSpan.TicksPerMillisecond;
		float secondsLeft = (float)(msToWait - m) / 1000f;

		if (secondsLeft < 0) {
			
			timerText.text = "Open me!";
			return true;
		} 
		
		return false;

	}




}
