﻿/*/Background erweitern wie du vorgehst:

1. Du erweiterst in der Update Funktion unter dem comment : "Checkt welcher Background aktiv sein soll", den code und fügst dem GameObject BackgroundRandom dein Background hinzu
2. Du erweiterst den images Array vom BackgroundRandom Object im Inspector und fügst dort das BackgroundImage ein was im Backgrounds Auwahl Menü zu sehen ist.
3. Du gehst in das SpawnManager Script und erweiterst in der Update Funktion unter dem comment: "fürs anpassen von verschiedenen cars zu den Backgrounds" den code und erstellst einen neuen Array für die Vehicle Typen
4  Erstell ein GameObject deiner Vehicle und Animiere sie IM INSPECTOR! und packe sie danach als Prefab in dein Project Browser. 
5. Füge dein Prefab nun Im inspector einem SpawnManager in dem jeweiligen Array zu
*/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class GetBackground : MonoBehaviour {


	[Header("Arraylist")]
	private List<GameObject> models;
	private int selectionIndex = 0;


	//private List<GameObject> models;
	//private int selectionIndex = 0; // Default Index of the Player

	// Use this for initialization
	void Start () {

		//////////////////////////////////////////////////////////////////
		/// Beim starten der Funktion wird "models" eine Liste zugewissen
		models = new List <GameObject> ();

		//Jedes Child Object wird in die Liste "models" gepackt
		foreach (Transform t in transform) {

			models.Add (t.gameObject); //wird in die Liste "models" gepackt
			t.gameObject.SetActive (false); //deaktiviert alle Childs
		}

		models [selectionIndex].SetActive (true); //aktiviert das Default (Also erstes Object in der List)
		/////////////////////////////////////////////////////////////////////
		/// 

		//Background wird dem PlayerContainer index angepasst. Daher Background muss auch genau den selben siblingindex haben
		Select (PlayerContainer.selectionIndex);
	}

	// Update is called once per frame
	void Update () {



        //hier Array für Neue Backgrounds erwitern und code duplizieren

    } /////////////////////////////////////////////////////////////////////
	/// Funktion zur Auswahl von einem Object aus der Liste (ChildObject)
	public void Select (int index){

		//Wenn Index gleich selectionIndex ist dann gib den wert zurück (also mach nichts)
		if (index == selectionIndex) {

			return;
		}

		//Wenn Index kleiner oder wenn Index größer/gleich der Anzahl der Liste ist gib den Wert zurück
		if (index < 0 || index >= models.Count) {

			return;
		}

		models [selectionIndex].SetActive (false);
		selectionIndex = index;
		models [selectionIndex].SetActive (true);


	}
	////////////////////////////////////////////////////////////////////
}

