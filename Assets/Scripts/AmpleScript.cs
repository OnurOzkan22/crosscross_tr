﻿using UnityEngine;
using System.Collections;

public class AmpleScript : MonoBehaviour {

	[Header("Einbindung ZugSpawnManager")]
	public GameObject zugSpawnManager;
	public ZugSpawnManager zugScript;
	public Animator anim;
	public string[] whichAnim;

	// Use this for initialization
	void Start () {
	
		zugSpawnManager = GameObject.FindGameObjectWithTag ("ZugSpawnManager");
		zugScript = zugSpawnManager.GetComponent<ZugSpawnManager> ();

		anim = GetComponent<Animator> ();

	}
	
	// Update is called once per frame
	void Update () {
	
		if (zugScript.ampelSchalten == true) {
		
			anim.Play (whichAnim[0]);
		} else if (zugScript.ampelSchalten == false) {

			anim.Play (whichAnim[1]);
		} 
	}
}
