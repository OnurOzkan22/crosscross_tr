﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//Einbindung in PlayerMovement wenn Player mit Car Kollidiert //Kleiner Bug abund zu kann man 2 mal sterben!

public class ExtraLife : MonoBehaviour {

	public GameObject playerContainer;
	public PlayerContainer script;
	public Text priceText;
	public int price;
	public Button button;

	// Use this for initialization
	void Start () {
		
		playerContainer = GameObject.FindGameObjectWithTag ("PlayerContainer");
		script = playerContainer.GetComponent<PlayerContainer> ();
		priceText.text = price + " C";
		button = GetComponent<Button> ();
	}

	// Update is called once per frame
	void Update () {

	}

	public void OnDown(){

		Debug.Log ("YES");

		if (script.coins >= price) {

			button.interactable = false;
			script.coins -= price;
			script.playerLife += 1; //Nur der Wert der angezeigt werden soll
			script.deathCounter = 1; //Death Counter wird aufgeladen
			PlayerPrefs.SetFloat ("Coins", script.coins);

		}
	}
}
