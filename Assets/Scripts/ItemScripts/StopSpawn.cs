﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//Script wurde eingebunden in PlayerContainer ObjectScript in der Update Funktion


public class StopSpawn : MonoBehaviour {

	public GameObject playerContainer;
	public PlayerContainer script;
	public Button button;

	public GameObject startButton;
	public StartButton startScript;

	public int price;
	public Text priceText;

	void Start(){

		startButton = GameObject.FindGameObjectWithTag ("StartButton");
		startScript = startButton.GetComponent<StartButton>();

		playerContainer = GameObject.FindGameObjectWithTag ("PlayerContainer");
		script = playerContainer.GetComponent<PlayerContainer> ();

		button = GetComponent<Button> ();
		priceText.text = price + " C";

		}


	public void StopSpawning(){

		if (script.coins >= price) {

			button.interactable = false;
			script.coins -= price;
			startScript.stopStart = true;
			//gameObject.SetActive (false);
			PlayerPrefs.SetFloat ("Coins", script.coins);
		}



	}
}
