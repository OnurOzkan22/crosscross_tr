﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoreTimeItem : MonoBehaviour {

	public GameObject playerContainer;
	public PlayerContainer script;

	public int price;
	public int getThisTime;
	public Text priceText;
	public Button button;

	// Use this for initialization
	void Start () {
	
		playerContainer = GameObject.FindGameObjectWithTag ("PlayerContainer");
		script = playerContainer.GetComponent<PlayerContainer> ();
		priceText.text = price + " C" ;
		button = GetComponent<Button> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnDown(){

		if (script.coins >= price) {
		
			button.interactable = false;
			script.gameTimer += getThisTime;
			script.coins -= price;
			PlayerPrefs.SetFloat ("Coins", script.coins);
		}
	}
}
