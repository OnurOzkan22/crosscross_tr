﻿using UnityEngine;
using System.Collections;

public class SpeedAnzeige : MonoBehaviour {

	[Header("Einbindung PlayerScript")]
	public GameObject playerContainer;
	public PlayerContainer script;

	public GameObject dreiSchild;
	public SpeedLimit dreiSchildScript;

	public Animator speedAnim;

	public bool findSpeedLimit = false;

	// Use this for initialization
	void Start () {

		speedAnim = GetComponent<Animator> ();
	
		playerContainer = GameObject.FindGameObjectWithTag ("PlayerContainer");
		script = playerContainer.GetComponent<PlayerContainer> (); // ändern zu PlayerContainer


	
	}
	
	// Update is called once per frame
	void Update ()
	{


		speedAnim.SetFloat("score", script.score);

		if (findSpeedLimit == true) {

			speedAnim.SetBool ("slow", dreiSchildScript.slow);
		}


	}

	public void Find(){

		dreiSchild = GameObject.FindGameObjectWithTag ("SpeedLimit");
		dreiSchildScript = dreiSchild.GetComponent<SpeedLimit> (); // ändern zu PlayerContainer
		findSpeedLimit = true;
	}
}

	

