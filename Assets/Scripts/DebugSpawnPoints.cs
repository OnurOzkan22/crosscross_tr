﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DebugSpawnPoints : MonoBehaviour {

	void OnDrawGizmos(){

		foreach (Transform t in transform) {

			Gizmos.color = Color.blue;

			if (t.transform.position.y < 0) {

				Gizmos.DrawLine (t.transform.position, new Vector2 (t.transform.position.x, t.transform.position.y + 15));
			
			} else {
			
				Gizmos.DrawLine (t.transform.position, new Vector2 (t.transform.position.x, t.transform.position.y - 15));
			}
		}
	}
}
