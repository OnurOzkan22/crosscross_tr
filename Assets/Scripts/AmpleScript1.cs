﻿using UnityEngine;
using System.Collections;

public class AmpleScript1 : MonoBehaviour {

	[Header("Einbindung ZugSpawnManagerUp")]
	public GameObject zugSpawnManager;
	public ZugSpawnManager1 zugScript;
	public Animator anim;
	public string[] whichAnim;

	// Use this for initialization
	void Start () {
	
		zugSpawnManager = GameObject.FindGameObjectWithTag ("ZugSpawnManagerUp");
		zugScript = zugSpawnManager.GetComponent<ZugSpawnManager1> ();

		anim = GetComponent<Animator> ();

	}
	
	// Update is called once per frame
	void Update () {
	
		if (zugScript.ampelSchalten == true) {
		
			anim.Play (whichAnim[0]);
		} else if (zugScript.ampelSchalten == false) {

			anim.Play (whichAnim[1]);
		} 
	}
}
