﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class PlayerMovement : MonoBehaviour {


	[Header("Leveling")]
	public int levelCounter;
	public int level;
	public float barFillAmount;
	public GameObject buttonPanel;
	[Header("Player Animator")]
	public Animator playerAnim;
	public SpriteRenderer rend;
	[Header("Touch Control")]
	[Tooltip("Aktiviert Touch Control")]
	public static bool touchStart = false;
	[Header("Game Over Screen")]
	[Tooltip("Bindet Restart Button ein und löst ihn aus")]
	public GameObject restartButton;
	[Header("Einbindung PlayerContainer")]
	public GameObject playerContainer;
	public PlayerContainer script;
	[Header("Einbindung StartButton")]
	public GameObject startButton;
	public StartButton startScript;
	[Tooltip("hier eingeben welchen Speed der Player nach der Idle haben soll. Eingeben welchen Speed der Player vorher hatte! bsp playerSpeed = -7 dann afterIldeSpeed = -7")]
	public float afterIdleSpeed; 
	public float getPlayerSpeed;
	public bool walkNow;
	[Header("SecondLife Option")]
	public GameObject ShowSecondLifePanel;
	public GameObject secondLife;
	public Text secondLifeText;
	public float timerSecondLife = 3;
	public bool startSecondLifeTimer = false;
	[Header("CoinsFeedback")]
	public GameObject dollarsPlus1;
	public GameObject goldNuggetsPlus1;
	public BoxCollider2D box;
	public GameObject stopPointContainer;
	public bool dontStop = false;
	public Transform[] stopPoint;



	// Use this for initialization
	void Start () {

		buttonPanel = GameObject.Find ("ButtonPanel");

		box = GetComponent<BoxCollider2D> ();

		rend = GetComponent<SpriteRenderer> ();

		playerContainer = GameObject.FindGameObjectWithTag ("PlayerContainer");
		script = playerContainer.GetComponent<PlayerContainer> ();

		startButton = GameObject.FindGameObjectWithTag ("StartButton");
		startScript = startButton.GetComponent<StartButton> ();
	
		playerAnim = GetComponent<Animator> ();

		levelCounter = PlayerPrefs.GetInt (gameObject.name + "levelCounter");
		level = PlayerPrefs.GetInt (gameObject.name + "level");
		barFillAmount = PlayerPrefs.GetFloat (gameObject.name + "bar");

		stopPoint = new Transform[stopPointContainer.transform.childCount];

		for (int i = 0; i < stopPoint.Length; i++) {
		
			stopPoint [i] = stopPointContainer.transform.GetChild (i);
		}
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		#region code in Update

		///Vorwarnung für extra Life 
		playerAnim.SetBool ("speed", walkNow);
		playerAnim.SetInteger ("ThisLevel", level);

		///Touch Steuerung
		if (touchStart == true/*aktiviert TouchControl*/) {
			foreach (Touch touch in Input.touches) {

				if (touch.phase == TouchPhase.Began) {

					getPlayerSpeed = afterIdleSpeed;
					walkNow = true;
				}
		
			} 
		} 
	
			///////////////////////////////////////////////////////////////////////////////////////////////	

			if (startSecondLifeTimer == true) {

				secondLifeText.text = "in: " + Mathf.Round (timerSecondLife);
				playerContainer.transform.position = new Vector2 (9.14f, -0.92f); //Heftet player an diese position
				timerSecondLife -= Time.deltaTime;


				if (1 > timerSecondLife) {
					timerSecondLife = 0;
					script.playerSpeed = afterIdleSpeed;
					ShowSecondLifePanel.SetActive (false);
					startSecondLifeTimer = false;
				}
			}
			////////////////////////////////////////////////////////////////////////////////////////////////

			//spezifischer speed wird an den Player Container übergeben
			if (startScript.startPlayerContainer == true) {

				script.playerSpeed = getPlayerSpeed;

			}
			//////////////////////////////////////////////////////////////



			///Spielt extra Anim ab wenn Player durch Zeit verloren hat
			if (1 > script.gameTimer) {
		
				//Hier noch animation für Death nach Time out einfügen
			}
			///////////

			//Debug.Log (script.deathCounter);

			#endregion
}



	//Collisions Erkennung vom Player
	void OnCollisionEnter2D (Collision2D coll)
	{
	
		if (coll.gameObject.tag == "Car") {

			//Führt den Block aus wenn Leben runtergezählt wurde
			if (script.deathCounter < 1) {

				box.enabled = false;
				rend.sortingOrder = -2;
				startScript.objects [8].SetActive (false); //deaktiviert TouchToStop
				//touchStart = false; // deaktiviert TouchControl
				Debug.Log ("YES");
				getPlayerSpeed = 0;
				script.startTimer = false;
				script.playerLife = 0;
				//Saved LevelDaten
				PlayerPrefs.SetInt (gameObject.name + "levelCounter" , levelCounter);
				PlayerPrefs.SetInt (gameObject.name + "level", level);
				buttonPanel.transform.GetChild (gameObject.transform.GetSiblingIndex ()).GetComponent<SelectPlayer> ().thislevel = level;
				PlayerPrefs.SetInt (buttonPanel.transform.GetChild (gameObject.transform.GetSiblingIndex ()).name + "MenuImageAnim", buttonPanel.transform.GetChild (gameObject.transform.GetSiblingIndex ()).GetComponent<SelectPlayer> ().thislevel);
			
	
				//startet Game Over Screen
				script.GameOver ();


				//Checkt auf welcher Seite der Straße der Player ist um je nach dem eine Anim abzuspielen
				if (transform.position.x > 0 && script.deathCounter == 0) {
					playerAnim.Play ("PlayerDeath_Down");

				}

				//Checkt auf welcher Seite der Straße der Player ist um je nach dem eine Anim abzuspielen
				if (transform.position.x < 0 && script.deathCounter == 0) {
					playerAnim.Play ("PlayerDeath_Up");
					

				}

				//Wenn leben nicht runtergezählt ist dann bleibt spieler am Leben
			} else if (script.deathCounter > 0) { 

				ShowSecondLifePanel.SetActive(true);
				secondLife = GameObject.FindGameObjectWithTag ("ExtraLife");
				secondLifeText = secondLife.GetComponent<Text> ();
				startSecondLifeTimer = true; // startet den SecondScreenAnzeige Timer
				script.deathCounter -= 1;
				TurnOnAllStopPointsCollider ();

			}



		}
	}

	public void AddLevelPoint(){

		levelCounter++;

		if (levelCounter % 10 == 0 && level < 10) {

			level++;
			PlayerPrefs.SetInt (gameObject.name + "level", level);
			barFillAmount += 0.1f;
			PlayerPrefs.SetFloat (gameObject.name + "bar", barFillAmount);

		} 

	}

	//Checkt das einsammeln der Coins
		void OnTriggerEnter2D (Collider2D coll)
	{

		if (coll.gameObject.tag == "GoldNugget") {
		
			Debug.Log ("yes hit");
			script.coins += 3;
			script.coinsCollected++;
			Destroy (coll.gameObject);
			Instantiate (goldNuggetsPlus1, transform.position, Quaternion.identity);
		}

		if (coll.gameObject.tag == "Coin") {


			script.coins++;
			script.coinsCollected++;
			Destroy (coll.gameObject);
			Instantiate (dollarsPlus1, transform.position, Quaternion.identity);

		}

		if (coll.gameObject.tag == "StopPoint" /*&& dontStop == false*/) {

			getPlayerSpeed = 0;
			walkNow = false;
			dontStop = false;
			coll.gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			transform.parent.position = new Vector2 (coll.gameObject.transform.position.x + 0.10f, transform.parent.position.y);
		}

	}

	public void TurnOnAllStopPointsCollider(){

		foreach (Transform t in stopPointContainer.transform) {
		
			t.gameObject.GetComponent<BoxCollider2D> ().enabled = true;
		}
	}

	public void TurnOffAllStopPointsCollider(){

		foreach (Transform t in stopPointContainer.transform) {

			t.gameObject.GetComponent<BoxCollider2D> ().enabled = false;
		}
	}


}
	//////////////////////////////////////////////////////////////////////////////////////////

	



