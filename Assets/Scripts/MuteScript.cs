﻿using UnityEngine;
using System.Collections;

public class MuteScript : MonoBehaviour {

	public AudioListener listener;
	public bool setMute;
	public float saveMute = 1;
	public Animator anim;

	// Use this for initialization
	void Start () {

		saveMute = PlayerPrefs.GetFloat ("saveMute");
		anim = GetComponent<Animator> ();

		if (saveMute == 1) { 

			AudioListener.pause = true;
			Debug.Log("MUTE");
			anim.Play ("MuteOnAnim");

		} 

		if (saveMute == 0) {

			AudioListener.pause = false;
			anim.Play ("MuteOffAnim");

		}
			
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnClick(){
	
		if (AudioListener.pause == false) {
		//Mute
			AudioListener.pause  = true;
			anim.Play ("MuteOnAnim");
			saveMute = 1;

		} else if (AudioListener.pause  == true) {
		//Not Mute

			AudioListener.pause = false;
			anim.Play ("MuteOffAnim");
			saveMute = 0;
		}

		PlayerPrefs.SetFloat ("saveMute", saveMute);
	}
}
