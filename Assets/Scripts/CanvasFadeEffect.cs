﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CanvasFadeEffect : MonoBehaviour {

	public GameObject[] objects;
	public Text[] text;
	public bool startPlayerContainer = false;
	public bool stopStart = false;
	public bool startTimerStopSpawn = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}



	public void OnClick (){

		startTimerStopSpawn = true;

		if (stopStart == false) {
			objects [0].SetActive (true);//SpwanManager
			objects [1].SetActive (true);//SpwanManager
			objects [2].SetActive (true);//SpwanManager
			objects [3].SetActive (true);//SpwanManager
			objects [4].SetActive (true);

		} else if (stopStart == true) {

			objects [0].SetActive (false);//SpwanManager
			objects [1].SetActive (false);//SpwanManager
			objects [2].SetActive (false);//SpwanManager
			objects [3].SetActive (false);//SpwanManager
			//objects [4].SetActive (false);

		}


		objects [5].SetActive (true);

		///UI Elemente
		objects [6].SetActive (true);
		objects [7].SetActive (true);


		text [0].enabled = true;
		text [1].enabled = true;
		text [2].enabled = true;
		text [3].enabled = false;
		startPlayerContainer = true;
		gameObject.SetActive (false);

	}
}
