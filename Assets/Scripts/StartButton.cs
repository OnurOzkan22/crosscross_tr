﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StartButton : MonoBehaviour {

	[Header("Einbindung Player Container")]
	public GameObject player;
	public PlayerMovement playerScript;
	[Header("alle GameObject die hier Aktiviert/Deaktiviert werden")]
	public GameObject[] objects;
	[Header("alle Texte die hier Aktiviert/Deaktiviert werden")]
	public Text[] text;
	[Header("Aktiviert das PlayerContainer GameObject")]
	[Tooltip("siehe in PlayerContainer.cs")]
	public bool startPlayerContainer = false;
	[Header("Wird Aktiviert wenn StopSpawn Item gekauft wurde")]
	[Tooltip("siehe hier im Script")]
	public bool stopStart = false;
	[Tooltip("Timer solang kommen keine Vehicle")]
	public bool startTimerStopSpawn = false;
	[Header("Startet Animationen mit dem Script AnimControllerScript.cs")]
	public bool startPanelAnim = false;
	[Header("Diese variable schaut ob der Restart Button oder Home button betätigt wurde")]
	public float directRestart;

	void Start(){

		player = GameObject.FindGameObjectWithTag ("Player");
		playerScript = player.GetComponent<PlayerMovement> ();

		//Variable für direkten Restart ohne zum Hauptmenü zu gelangen(0 --> Hauptmenü/ 1 --> Restart
		directRestart = PlayerPrefs.GetFloat ("directRestart");

		if (directRestart == 1) {

			OnClick ();
			directRestart = 0;
			PlayerPrefs.SetFloat ("directRestart",directRestart);
		}
		//////////////////////////////////////////////////////////////////////////////////////////////////
	}

	void Update (){


	}

	public void OnClick (){

		PlayerContainer.ready = true;
		//objects [13].GetComponent<GraphicRaycaster> ().enabled = false; // verhindert nach start UI interaction
		PlayerMovement.touchStart = true; // aktiviert die touch steuerung
		startTimerStopSpawn = true; //Startet den Timer für das STOP SPAWN ITEM
		objects [12].transform.position = new Vector2 (8, -0.97f); //positioniert den PlayerContainer an dieser Position

		objects [5].SetActive (true); //CoinsManager
		objects [9].SetActive (true); //SpeedAnzeige
		objects [14].SetActive(true);
		objects [6].SetActive(true);

		//Item StopStart
		if (stopStart == false) {
			objects [0].SetActive (true);//SpwanManager
			objects [1].SetActive (true);//SpwanManager
			objects [2].SetActive (true);//SpwanManager
			objects [3].SetActive (true);//SpwanManager
			objects [4].SetActive (true); //3oiger Item
			objects[0].GetComponentInParent<SpawnContainer>().SetSpawnManagerPos();
			startPanelAnim = true; //Startet Animation die zum Spiel überführt beim start drücken (siehe AnimControllerScript.cs)



		} else if (stopStart == true) {
		
			objects [0].SetActive (false);//SpwanManager
			objects [1].SetActive (false);//SpwanManager
			objects [2].SetActive (false);//SpwanManager
			objects [3].SetActive (false);//SpwanManager
			objects [4].SetActive (false);
			startPanelAnim = true; //Startet Animation die zum Spiel überführt beim start drücken


		}
			
		///////////////////////////////////////////////////////////////////////////
	
		//Texte hier einfügen
		text [0].enabled = true;
		text [2].enabled = true;
		text [3].enabled = false;
		startPlayerContainer = true; //startet den PlayerContainer und somit den Timer der den Run startet (siehe PlayerContainer.cs)
		///////////////////////////////////////////////////////////////////////////


	}
}
