﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Highscore : MonoBehaviour {


	public GameObject playerContainer;
	public PlayerContainer script;
	public Text highscoreText;
	public Text startHighscoreText;
	public Text newHighscoreText;
	public float highscore;
	public bool newHighscoreAppear = false;


	// Use this for initialization
	void Start () {
	
		playerContainer = GameObject.FindGameObjectWithTag ("PlayerContainer");
		script = playerContainer.GetComponent<PlayerContainer> (); 
	
		highscore = PlayerPrefs.GetFloat ("Highscore");

		startHighscoreText.text = "BEST: " + highscore; // Highscore Anzeige im Shop
	}

	void Update(){


		//Wenn Player mit Car Kollidiert
		if (script.getHighscore == true) {
		
			highscoreText.enabled = true;
			highscoreText.text = "" + highscore;

			//Wenn score größer ist als Highscore übernehme neuen Highscore
			if (script.score > highscore ) {


				SaveHighscore ();
				highscoreText.text = "" + highscore;

			}
		}

		//Wenn Timer Null ist Highscore Speichern
		if (script.getHighscore == true) {

			highscoreText.enabled = true;
			highscoreText.text = "" + highscore;
		

			//Wenn score größer ist als Highscore übernehme neuen Highscore
			if (script.score > highscore ) {

				newHighscoreAppear = true;
				SaveHighscore ();
				highscoreText.text = "" + highscore;

			}
		}
			
	}


	void SaveHighscore() {
	
		newHighscoreText.enabled = true;
		newHighscoreText.text = "NEW HIGHSCORE";
		highscore = script.score;
		PlayerPrefs.SetFloat ("Highscore", highscore); //mache Variable highscore zum Key Highscore
	}

	

}
