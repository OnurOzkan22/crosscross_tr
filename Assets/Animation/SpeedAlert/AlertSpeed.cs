﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlertSpeed : MonoBehaviour {

	public Text alertText;

	// Use this for initialization
	void Start () {

		alertText = GetComponent<Text> ();
		alertText.text = "Carspeed: 9000km/h!!";
		if (PlayerContainer.scoreForAlert > 7 && PlayerContainer.scoreForAlert < 17 ) {
		
			alertText.text = "Carspeed: 9000km/h!!";

		} else if (PlayerContainer.scoreForAlert > 17) {

			alertText.text = "Carspeed: 120km/h!!";
		}
	}
	
	public void SetActive(){
	
		gameObject.SetActive (false);
	
	}
}
