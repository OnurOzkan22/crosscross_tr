﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemShop : MonoBehaviour {

	public GameObject openButton;
	public GameObject exitButton;
	public GameObject itemShop;
	public Animator anim;
	public GameObject selectPanel;
	public ScrollRect rect;
	// Use this for initialization
	void Start () {

		anim = itemShop.GetComponent<Animator> ();

		rect = selectPanel.GetComponent<ScrollRect> ();

		anim.Play ("StartAnim");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnClick(){
	
		anim.Play ("ItemShopAnim");
		openButton.SetActive (false);
		exitButton.SetActive (true);
		rect.enabled = false;

	}

	public void Exit(){
	
		anim.Play ("ItemAnimOut");
		openButton.SetActive (true);
		exitButton.SetActive (false);
		rect.enabled = true;
	}
}
